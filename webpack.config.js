var path = require('path');
var fs = require('fs');
var externals = {};

fs.readdirSync('node_modules').filter(function (x) {
  return ['.bin'].indexOf(x) === -1;
}).forEach(function (mod) {
  externals[mod] = 'commonjs ' + mod;
});

module.exports = {
  entry: ['babel-polyfill', './src/index.js'],
  output: {
    path: __dirname,
    filename: 'index.js'
  },
  target: 'node',
  module: {
    loaders: [{
      loader: 'babel-loader',
      include: [path.resolve(__dirname, 'src')],
      test: /\.js$/,
      query: {
        presets: ['es2015', 'stage-0']
      }
    }]
  },
  externals: externals
};

