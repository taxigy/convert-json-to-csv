/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(1);
	module.exports = __webpack_require__(2);


/***/ },
/* 1 */
/***/ function(module, exports) {

	module.exports = require("babel-polyfill");

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

	var _lodash = __webpack_require__(3);

	var _lodash2 = _interopRequireDefault(_lodash);

	var _readline = __webpack_require__(4);

	var _readline2 = _interopRequireDefault(_readline);

	var _fs = __webpack_require__(5);

	var _fs2 = _interopRequireDefault(_fs);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var columnsShown = null;
	var argv = _lodash2.default.slice(process.argv, 2);
	var file = _lodash2.default.first(argv);
	var reader = _readline2.default.createInterface({
	  input: _fs2.default.createReadStream(file)
	});

	function columns(data) {
	  var prefix = arguments.length <= 1 || arguments[1] === undefined ? '' : arguments[1];

	  if (_lodash2.default.isArray(data)) {
	    return columns(_lodash2.default.first(data), prefix + '.#');
	  } else if (_lodash2.default.isObject(data)) {
	    return _lodash2.default.filter(_lodash2.default.flatMap(_lodash2.default.keys(data), function (key) {
	      return _lodash2.default.concat(!_lodash2.default.isObject(_lodash2.default.result(data, key)) ? prefix + '.' + key : null, columns(data[key], prefix + '.' + key));
	    }));
	  } else {
	    return null;
	  }
	}

	function flattenKeys(original, keys) {
	  return _lodash2.default.mapValues(_lodash2.default.groupBy(_lodash2.default.map(keys, function (key) {
	    return {
	      key: key,
	      value: _lodash2.default.result(original, key)
	    };
	  }), 'key'), function (e) {
	    return _lodash2.default.result(e, '0.value');
	  });
	}

	function unfold(original, keys, iterables) {
	  if (!_lodash2.default.isArray(iterables)) {
	    return unfold(original, keys, _lodash2.default.uniq(_lodash2.default.map(_lodash2.default.filter(keys, function (key) {
	      return (/#/i.test(key)
	      );
	    }), function (key) {
	      return _lodash2.default.replace(key, /\.\#.*/, '');
	    })));
	  } else if (_lodash2.default.isEmpty(iterables)) {
	    return [];
	  } else {
	    var _ret = function () {
	      var first = _lodash2.default.first(iterables);
	      var rest = _lodash2.default.tail(iterables);
	      var values = _lodash2.default.result(original, first);
	      var flatKeys = _lodash2.default.mapKeys(_lodash2.default.filter(keys, function (key) {
	        return _lodash2.default.includes(key, first);
	      }));
	      var updated = _lodash2.default.map(values, function (v, i) {
	        return _extends({}, flattenKeys(original, keys), _lodash2.default.mapValues(flatKeys, function (k) {
	          return _lodash2.default.result(original, _lodash2.default.replace(k, /\#/, i));
	        }));
	      });

	      return {
	        v: _lodash2.default.concat(updated, unfold(original, keys, rest))
	      };
	    }();

	    if ((typeof _ret === 'undefined' ? 'undefined' : _typeof(_ret)) === "object") return _ret.v;
	  }
	}

	function normalize(originals, keys) {
	  return _lodash2.default.map(originals, function (original) {
	    return _lodash2.default.map(keys, function (key) {
	      return _lodash2.default.result(original, key);
	    });
	  });
	}

	reader.on('line', function (line) {
	  var data = JSON.parse(line);
	  var unfolded = unfold(data, columns(data));
	  var normalized = normalize(unfolded, columns(data));

	  if (!columnsShown) {
	    console.log(_lodash2.default.join(columns(data), ','));
	    columnsShown = true;
	  }

	  var _iteratorNormalCompletion = true;
	  var _didIteratorError = false;
	  var _iteratorError = undefined;

	  try {
	    for (var _iterator = normalized[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
	      var e = _step.value;

	      console.log(_lodash2.default.join(e, ','));
	    }
	  } catch (err) {
	    _didIteratorError = true;
	    _iteratorError = err;
	  } finally {
	    try {
	      if (!_iteratorNormalCompletion && _iterator.return) {
	        _iterator.return();
	      }
	    } finally {
	      if (_didIteratorError) {
	        throw _iteratorError;
	      }
	    }
	  }
	});

/***/ },
/* 3 */
/***/ function(module, exports) {

	module.exports = require("lodash");

/***/ },
/* 4 */
/***/ function(module, exports) {

	module.exports = require("readline");

/***/ },
/* 5 */
/***/ function(module, exports) {

	module.exports = require("fs");

/***/ }
/******/ ]);