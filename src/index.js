import _ from 'lodash';
import readline from 'readline';
import fs from 'fs';

let columnsShown = null;
const argv = _.slice(process.argv, 2);
const file = _.first(argv);
const reader = readline.createInterface({
  input: fs.createReadStream(file)
});

function columns(data, prefix = '') {
  if (_.isArray(data)) {
    return columns(_.first(data), `${prefix}.#`);
  } else if (_.isObject(data)) {
    return _.filter(_.flatMap(_.keys(data), key => {
      return _.concat(
        !_.isObject(_.result(data, key)) ? `${prefix}.${key}` : null,
        columns(data[key], `${prefix}.${key}`)
      );
    }));
  } else {
    return null;
  }
}

function flattenKeys(original, keys) {
  return _.mapValues(_.groupBy(_.map(keys, key => ({
    key,
    value: _.result(original, key)
  })), 'key'), e => _.result(e, '0.value'));
}

function unfold(original, keys, iterables) {
  if (!_.isArray(iterables)) {
    return unfold(original, keys, _.uniq(_.map(_.filter(keys, key => /#/i.test(key)), key => _.replace(key, /\.\#.*/, ''))));
  } else if (_.isEmpty(iterables)) {
    return [];
  } else {
    const first = _.first(iterables);
    const rest = _.tail(iterables);
    const values = _.result(original, first);
    const flatKeys = _.mapKeys(_.filter(keys, key => _.includes(key, first)));
    const updated = _.map(values, (v, i) => ({
      ...flattenKeys(original, keys),
      ..._.mapValues(flatKeys, k => _.result(original, _.replace(k, /\#/, i)))
    }));

    return _.concat(updated, unfold(original, keys, rest));
  }
}

function normalize(originals, keys) {
  return _.map(originals, original => _.map(keys, key => _.result(original, key)));
}

reader.on('line', line => {
  const data = JSON.parse(line);
  const unfolded = unfold(data, columns(data));
  const normalized = normalize(unfolded, columns(data));

  if (!columnsShown) {
    console.log(_.join(columns(data), ','));
    columnsShown = true;
  }

  for (const e of normalized) {
    console.log(_.join(e, ','));
  }
});
